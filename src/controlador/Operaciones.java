/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author eds
 */
public class Operaciones {

    //METODO PARA LEER EL PROCESO A EJECUTAR
    public String leerProceso(Process p) {
        
        String str="";
        BufferedReader bufferStream = null;
        
        try {
            bufferStream = new BufferedReader(
            new InputStreamReader(p.getInputStream()));
            String line = bufferStream.readLine();
            while(line != null){
                str += line +"\n";
                line = bufferStream.readLine();
            }
        } catch (IOException e) {
            System.err.println("Error al leer archivo");
        }
        return str;
    }
    
    //METODO PARA EJECUTAR EL COMANDO DE LA TERMINAL
    public Process execCommand(String cmd) {
        
        Process po = null;
        try {
            String [] command = {"bash", "-c",cmd};
            po = Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            System.err.println("Error al ejecutar");
        }
        return po;        
    }
}