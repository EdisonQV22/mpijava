/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author eds
 */
public class ControladorArchivos extends JFrame{

    public void abrirArchivo(String FileOpen ) {
       FileOpen = "";
       JFileChooser file = new JFileChooser();
        file.showOpenDialog(this);
        /**
         * abrimos el archivo seleccionado
         */
        File abre = file.getSelectedFile();

        /**
         * recorremos el archivo, lo leemos para plasmarlo en el area de texto
         */
        if (abre != null) {
            FileOpen = abre.getAbsolutePath();
        } else {
            System.out.println("Error ");
        }
        
    }
}
